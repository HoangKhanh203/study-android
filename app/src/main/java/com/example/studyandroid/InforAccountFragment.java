package com.example.studyandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InforAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InforAccountFragment extends Fragment {
    EditText username;
    EditText Pass;
    EditText Email;
    EditText Name;
    Toolbar toolbar;
    RadioGroup radioGroup;
    RadioButton radioFemale;
    RadioButton radioMale;
    SharedPreferences sharedPreferences;
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public InforAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment inforaccount.
     */
    // TODO: Rename and change types and number of parameters
    public static InforAccountFragment newInstance(String param1, String param2) {
        InforAccountFragment fragment = new InforAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_inforaccount, container, false);
        username = view.findViewById(R.id.inforUsername);
        Email = view.findViewById(R.id.inforEmail);
        Pass = view.findViewById(R.id.inforPassword);
        Name = view.findViewById(R.id.inforName);
        radioGroup = view.findViewById(R.id.radioGroup);
        radioFemale = view.findViewById(R.id.radioFemale);
        radioMale = view.findViewById(R.id.radioMale);

        toolbar = view.findViewById(R.id.toolbar);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferences = getContext().getSharedPreferences("DATAACCOUNT", Context.MODE_PRIVATE);
        String ps = sharedPreferences.getString("PASSWORD", "");
        String mail = sharedPreferences.getString("EMAIL", "");
        String name = sharedPreferences.getString("NAME", "");
        username.setText(sharedPreferences.getString("USERNAME", ""));
        Pass.setText(ps);
        Email.setText(mail);
        Name.setText(name);
//        radioGroup.setChecked(sp.getBoolean("Male",false));
//        radioButton.setChecked(sp.getBoolean("Female", false));

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.save) {
            SharedPreferences.Editor editor;
            String user = username.getText().toString().trim();
            String password = Pass.getText().toString().trim();
            String name = Name.getText().toString().trim();
            editor = sharedPreferences.edit();
            editor.putString("USERNAME", user);
            editor.putString("PASSWORD", password);
            editor.putString("NAME", name);
            if (radioMale.isChecked()) {
                editor.putBoolean("Male", true);
                editor.putBoolean("Female", false);
            } else {
                editor.putBoolean("Male", false);
                editor.putBoolean("Female", true);
            }
            editor.commit();
            Toast.makeText(getActivity(), "Saved", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }
}