package com.example.studyandroid.Dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.studyandroid.Product;
import com.example.studyandroid.R;

import java.util.ArrayList;

public class DashboardAdapter extends ArrayAdapter<Product> {
    Context context;
    ArrayList<Product> arrayList;

    public DashboardAdapter(@NonNull Context context, @NonNull ArrayList<Product> object) {
        super(context, 0, object);
        this.context = context;
        this.arrayList = object;
    }

    @NonNull
    @Override
    public View getView(int postion, @Nullable View convertView, @Nullable ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        Product product = arrayList.get(postion);
        convertView = layoutInflater.inflate(R.layout.layout_item_dashboard, parent, false);

        TextView title = convertView.findViewById(R.id.title_view_grid);
        ImageView image = convertView.findViewById(R.id.image_grid);


        title.setText(product.getTitle());
        image.setImageBitmap(product.getImage());


        return convertView;
    }
}
