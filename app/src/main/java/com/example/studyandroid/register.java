package com.example.studyandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Pattern;

public class register extends AppCompatActivity {
    EditText inputEmail, inputUsername, inputPassword, inputConfirm;
    Button btnSignIn, btnCancel;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        inputEmail = findViewById(R.id.inputEmail);
        inputUsername = findViewById(R.id.username);
        inputPassword = findViewById(R.id.password);
        inputConfirm = findViewById(R.id.confirmpassword);

        btnSignIn = findViewById(R.id.btnSignin);
        btnCancel = findViewById(R.id.btnCancel);

        sharedPreferences = getSharedPreferences("DATAACCOUNT", Context.MODE_PRIVATE);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!CheckMail(inputEmail.getText().toString())) {
                    inputEmail.setError("Invalid email");
                    return;
                }
                ;
                if (inputUsername.getText().toString().isEmpty()) {
                    inputUsername.setError("Please input username...!");
                    return;
                }
                ;
                if (inputPassword.getText().toString().isEmpty()) {
                    inputPassword.setError("Please input Password...!");
                    return;
                }
                ;
                if (inputConfirm.getText().toString().isEmpty()) {
                    inputConfirm.setError("Please input Confirm Password...!");
                    return;
                }
                ;
                if (inputPassword.getText().toString().equals(inputConfirm.getText().toString())) {

                    Intent intent = new Intent(register.this, login.class);
                    intent.putExtra("USERNAME", inputUsername.getText().toString());
                    intent.putExtra("PASSWORD", inputPassword.getText().toString());

                    editor = sharedPreferences.edit();
                    editor.putString("USERNAME", inputUsername.getText().toString());
                    editor.putString("PASSWORD", inputPassword.getText().toString());
                    editor.putString("EMAIL", inputEmail.getText().toString());
                    editor.commit();
                    ;
                    setResult(101, intent);
                    finish();
                } else {
                    inputPassword.setError("Password and Confirm Password doesn't match");
                    inputConfirm.setText("");
                    return;
                }
            }
        });

    }

    public boolean CheckMail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }
}