package com.example.studyandroid;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class login extends AppCompatActivity {
    EditText editUserName, editPassword;
    Button btnLogin, btnRegister, btnOK, btnCancel;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUserName = findViewById(R.id.editUserName);
        editPassword = findViewById(R.id.editPass);
        Intent intent = getIntent();
        sharedPreferences = getSharedPreferences("DATAACCOUNT", Context.MODE_PRIVATE);
        Log.d("ge", String.valueOf(sharedPreferences.getString("USERNAME", "")));
        editUserName.setText(sharedPreferences.getString("USERNAME", ""));
        editPassword.setText(sharedPreferences.getString("PASSWORD", ""));


        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUserName.getText().toString().isEmpty() || editPassword.getText().toString().isEmpty()) {
                    final Dialog dialog = new Dialog(login.this);

                    dialog.setContentView(R.layout.dialogcustom);
                    btnOK = dialog.findViewById(R.id.btnOK);
                    btnCancel = dialog.findViewById(R.id.btnCancel);
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(login.this, register.class);
                            startActivityForResult(intent, 100);
                            dialog.dismiss();
                        }
                    });
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });
                    dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.WRAP_CONTENT);
                    dialog.show();
                } else if (editPassword.getText().toString().length() < 6) {
                    editPassword.setError("Minium 6 number");
                } else {
                    Intent intent = new Intent(login.this, fragment_main.class);
                    intent.putExtra("Username", editUserName.getText().toString());
                    startActivity(intent);
                }
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login.this, register.class);
                startActivityForResult(intent, 100);
            }
        });

    }
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        SharedPreferences sp= getSharedPreferences("DATAACCOUNT", Context.MODE_PRIVATE);
//        editUserName.setText(sp.getString("USERNAME", ""));
//        editPassword.setText(sp.getString("PASSWORD", ""));
//        if (requestCode == 100 && resultCode == 101) {
//            editUserName.setText(data.getStringExtra("username"));
//            editPassword.setText(data.getStringExtra("password"));
//        }
//        if (requestCode == 102 && resultCode == 101) {
//            editUserName.setText(data.getStringExtra("username"));
//            editPassword.setText(data.getStringExtra("password"));
//        }
//    }
}