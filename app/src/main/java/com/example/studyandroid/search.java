package com.example.studyandroid;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.gujun.android.taggroup.TagGroup;

public class search extends AppCompatActivity {

    SearchView searchView;
    ArrayList<Product> arrayList;
    Utils utils;
    ListView listView;
    ProductAdapter productAdapter;
    TagGroup mTagGroup;
    TagGroup tagGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        utils = new Utils(search.this);
        arrayList = new ArrayList<>();
        listView = findViewById(R.id.listView);
        productAdapter = new ProductAdapter(search.this, R.layout.layout_item, arrayList);
        listView.setAdapter(productAdapter);

        searchView = findViewById(R.id.search_vew);
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchFurniture(newText);
                return false;
            }
        });
        mTagGroup = findViewById(R.id.tag_group);
        mTagGroup.setTags(new String[]{"Kitchen", "Room", "Light", "Image", "HELLO"});
        mTagGroup.setOnTagClickListener(new TagGroup.OnTagClickListener() {
            @Override
            public void onTagClick(String tag) {
                searchView.setQuery(tag, false);
                hideSoftKeyboard(searchView);
            }
        });
    }

    private void searchFurniture(String newText) {
        ArrayList<Product> tmp = new ArrayList<>();
        JSONObject jsonData;
        try {
            jsonData = new JSONObject(Utils.getJsonFromAssets(getApplicationContext(), "dataMock.json"));
            JSONArray dataArr = jsonData.getJSONArray("data");

            for (int i = 0; i < dataArr.length(); i++) {
                JSONObject itemInArr = dataArr.getJSONObject(i);
                String title = itemInArr.getString("title");
                String description = itemInArr.getString("description");
                String urlImage = itemInArr.getString("urlImage");
                if (title.toUpperCase().contains(newText.toUpperCase())) {

                    tmp.add(new Product(title, description, Utils.convertStringToBitmapFromAccess(getApplicationContext(), urlImage)));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //        for (Product product : utils.LoadFileInternal()) {
//            if (product.getTitle().toLowerCase().contains(newText.toLowerCase())) {
//                tmp.add(product);
//            }
//        }
        Toast.makeText(this, tmp.size() + "", Toast.LENGTH_SHORT).show();
        if (tmp.size() > 0) {
            productAdapter.clear();
            productAdapter.addAll(tmp);
            productAdapter.notifyDataSetChanged();
            listView.setVisibility(View.VISIBLE);
        }
        if (newText.isEmpty()) {
            listView.setVisibility(View.GONE);
        }
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm
                = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}