package com.example.studyandroid.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.studyandroid.Product;
import com.example.studyandroid.R;

import java.util.ArrayList;

public class HomeAdapter extends ArrayAdapter<Product> {
    Context context;
    ArrayList<Product> arrayList;

    public HomeAdapter(@NonNull Context context, @NonNull ArrayList<Product> object) {
        super(context, 0, object);
        this.context = context;
        this.arrayList = object;
    }

    @NonNull
    @Override
    public View getView(int postion, @Nullable View convertView, @Nullable ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        Product product = arrayList.get(postion);
        convertView = layoutInflater.inflate(R.layout.layout_item, parent, false);

        TextView title = convertView.findViewById(R.id.title_view);
        TextView description = convertView.findViewById(R.id.description_view);
        ImageView image = convertView.findViewById(R.id.image_view);

        title.setText(product.getTitle());
        description.setText(product.getDescription());
        image.setImageBitmap(product.getImage());


        return convertView;
    }
}
