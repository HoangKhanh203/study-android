package com.example.studyandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter {

    Context context;
    ArrayList<Product> arrayList;
    int layoutresoure;

    public ProductAdapter(Context context, int layoutresoure, ArrayList<Product> arrayList) {

        this.context = context;
        this.layoutresoure = layoutresoure;
        this.arrayList = arrayList;
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.layout_item, null);
        Product product = this.arrayList.get(position);

        TextView tvtext = (TextView) convertView.findViewById(R.id.title_view);
        tvtext.setText(product.getTitle());

        TextView tvtext2 = (TextView) convertView.findViewById(R.id.description_view);
        tvtext2.setText(product.getDescription());

        ImageView img = (ImageView) convertView.findViewById(R.id.image_view);
        img.setImageBitmap(product.getImage());
        return convertView;
    }

    public void clear() {
        arrayList.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<Product> tmp) {
        arrayList = tmp;
        notifyDataSetChanged();
    }
}
