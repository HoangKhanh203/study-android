package com.example.studyandroid;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class inforActivity extends AppCompatActivity {

    EditText editname, editemail, editpass, editusername;
    RadioGroup rdbGender;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_inforaccount);
        sharedPreferences = getSharedPreferences("DATAACCOUNT", MODE_PRIVATE);
        editname = findViewById(R.id.inforName);
        editemail = findViewById(R.id.inforEmail);
        editpass = findViewById(R.id.inforPassword);
        editusername = findViewById(R.id.inforUsername);
        getSupportActionBar().setTitle("Account Info");
        editname.setText(sharedPreferences.getString("USERNAME", ""));
        editpass.setText(sharedPreferences.getString("PASSWORD", ""));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.nav_top, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String name = editusername.getText().toString().trim();
        String pass = editpass.getText().toString().trim();
        String email = editemail.getText().toString().trim();
        if (item.getItemId() == R.id.save) {
            editor = sharedPreferences.edit();
            editor.putString("USERNAME", name);
            editor.putString("PASSWORD", pass);
            editor.commit();
            // Intent intent=new Intent(infoActivity.this,Login.class);
            //intent.putExtra("Username", username);
            // intent.putExtra("Pass", pass);
            //setResult(104,intent);
            //finish();
        }

        return super.onOptionsItemSelected(item);
    }

}