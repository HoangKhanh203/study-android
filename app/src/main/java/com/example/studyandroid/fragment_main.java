package com.example.studyandroid;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.studyandroid.Dashboard.DashboardFragment;
import com.example.studyandroid.Home.HomeFragment;
import com.example.studyandroid.Notification.NotificationFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class fragment_main extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    Toolbar toolbarLayout;
    LinearLayout linearLayout;
    EditText searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_main);
        bottomNavigationView = findViewById(R.id.nav_bottom);
        toolbarLayout = findViewById(R.id.toolbar);
        linearLayout = findViewById(R.id.linearsearch);
        searchView = findViewById(R.id.search_view);
        displayFragment(R.id.mnuHome);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                displayFragment(item.getItemId());
                return true;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(fragment_main.this, search.class);
                startActivity(intent);
            }
        });
    }

    private void displayFragment(int itemNavBottom) {
        Fragment fragment = null;
        String titleBar = "Home";
        switch (itemNavBottom) {
            case R.id.mnuHome:
                fragment = new HomeFragment();
                linearLayout.setVisibility(View.VISIBLE);
                toolbarLayout.setVisibility(View.GONE);
                break;
            case R.id.mnuDashBoard:
                fragment = new DashboardFragment();
                titleBar = "DashBoard";
                linearLayout.setVisibility(View.VISIBLE);
                toolbarLayout.setVisibility(View.GONE);
                break;
            case R.id.mnuNoti:
                fragment = new NotificationFragment();
                titleBar = "Notification";
                linearLayout.setVisibility(View.VISIBLE);
                toolbarLayout.setVisibility(View.GONE);

                break;
            case R.id.mnuAccount:
                fragment = new InforAccountFragment();
                linearLayout.setVisibility(View.GONE);
                toolbarLayout.setVisibility(View.VISIBLE);
                //setSupportActionBar(toolbar);
                setSupportActionBar(toolbarLayout);
                toolbarLayout.setTitle("Account");
                break;

        }
//        getSupportActionBar().setTitle(titleBar);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContent, fragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.nav_top, menu);
        return super.onCreateOptionsMenu(menu);
    }
}