package com.example.studyandroid;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Utils {
    Context context;

    public Utils(Context context) {
        this.context = context;
    }

    public static ArrayList<Product> productHistory = new ArrayList<>();

    public static Bitmap convertStringToBitmapFromAccess(Context context, String
            filename) {
        AssetManager assetManager = context.getAssets();
        try {
            InputStream is = assetManager.open(filename);
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }


    static String filename = "database";

    public static ArrayList<Product> getProductHistory() {
        return productHistory;
    }

    public ArrayList<Product> LoadFileInternal() {
        ArrayList<Product> arrayList = new ArrayList<>();
        try {
            File file = new File(context.getFilesDir(), filename);
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            arrayList = (ArrayList<Product>) objectInputStream.readObject();
            Log.d("FurnitureApp", arrayList.size() + "");
            return arrayList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Product> getMockData(Context context) {
        ArrayList<Product> tmp = new ArrayList<>();
        JSONObject jsonData;
        try {
            jsonData = new JSONObject(getJsonFromAssets(context.getApplicationContext(), "dataMock.json"));
            JSONArray dataArr = jsonData.getJSONArray("data");

            for (int i = 0; i < dataArr.length(); i++) {
                JSONObject itemInArr = dataArr.getJSONObject(i);
                String title = itemInArr.getString("title");
                String description = itemInArr.getString("description");
                String urlImage = itemInArr.getString("urlImage");

                tmp.add(new Product(title, description, convertStringToBitmapFromAccess(context, urlImage)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        tmp.add(new Product(context.getString(R.string.product_one_title_10),context.getString(R.string.product_one_des_10), convertStringToBitmapFromAccess(context, "hinh_10.png")));
        return tmp;
    }

    public static ArrayList<Product> getMockDataDashboard(Context context) {
        ArrayList<Product> tmp = new ArrayList<>();
        JSONObject jsonData;
        try {
            jsonData = new JSONObject(getJsonFromAssets(context.getApplicationContext(), "dataMockDashboard.json"));
            JSONArray dataArr = jsonData.getJSONArray("data");

            for (int i = 0; i < 4; i++) {
                JSONObject itemInArr = dataArr.getJSONObject(i);
                String title = itemInArr.getString("title");
                String description = itemInArr.getString("description");
                String urlImage = itemInArr.getString("urlImage");

                tmp.add(new Product(title, description, convertStringToBitmapFromAccess(context, urlImage)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
//        tmp.add(new Product(context.getString(R.string.product_one_title_10),context.getString(R.string.product_one_des_10), convertStringToBitmapFromAccess(context, "hinh_10.png")));
        return tmp;
    }
}
